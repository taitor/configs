typeset -U path PATH
path=(
  /opt/homebrew/bin(N-/)
  $path
)

if [ -d "$HOME/.nvm" ]; then
  export NVM_DIR="$HOME/.nvm"
  [ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && . "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
  [ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && . "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
fi

export LC_CTYPE="UTF-8"

export XDG_CONFIG_HOME=$HOME/.config

autoload -Uz compinit
compinit

zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z} r:|[-_.]=**' 

setopt auto_pushd
setopt pushd_ignore_dups
 
HISTFILE=~/.zsh_history
HISTSIZE=1000000
SAVEHIST=1000000
setopt hist_ignore_dups
setopt share_history
setopt hist_ignore_space
setopt hist_verify
setopt hist_reduce_blanks  
setopt hist_save_no_dups
setopt hist_no_store
setopt hist_expand
setopt inc_append_history
bindkey -e
bindkey '^P' history-beginning-search-backward
bindkey '^N' history-beginning-search-forward
bindkey "^R" history-incremental-search-backward
bindkey "^S" history-incremental-search-forward

editor=$(which nvim)
if [ $? -eq 0 ]; then
  export EDITOR=$editor
else
  editor=$(which vim)
  if [ $? -eq 0 ]; then
    export EDITOR=$editor
  fi
fi
 
setopt print_eight_bit
 
setopt no_flow_control
 
setopt interactive_comments

autoload colors
colors

PROMPT="%{${fg[cyan]}%}%~%{${reset_color}%} 
[%n@$(uname -m)]$ "

PROMPT2='[%n]> '

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

if [[ ${ALACRITTY} == "true" && ${SHLVL} == "1" ]]; then
  (
    set -e
    command_name=ALACRITTY_THEME_AUTO_UPDATER
    if pgrep ${command_name} > /dev/null; then
      exit 0
    fi
    function theme_handler() {
      while read theme; do
        config_base=${XDG_CONFIG_HOME}/alacritty
        config_path=${config_base}/alacritty.toml
        config_content_path=${config_base}/alacritty-${theme}.toml
        cp ${config_content_path} ${config_path}
      done
    }
    (exec -a ${command_name} dark-notify < /dev/null | theme_handler) &
  )
fi
