if vim.fn.has('nvim') then
  vim.cmd("let $NVIM_TUI_ENABLE_TRUE_COLOR=1")
end
if vim.fn.has('termguicolors') then
  vim.opt.termguicolors = true
end

vim.opt.hidden = true
vim.opt.encoding = 'UTF-8'
vim.opt.fileencoding = 'UTF-8'
vim.opt.backup = false
vim.opt.writebackup = false
vim.opt.cmdheight = 2
vim.opt.updatetime = 300
vim.opt.signcolumn = 'yes:1'
vim.opt.expandtab = true
vim.opt.smarttab = true
vim.opt.autoindent = true
vim.opt.smartindent = true
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2
vim.opt.cursorline = true
vim.opt.number = true
vim.opt.showmatch = true
vim.opt.incsearch = true
vim.opt.completeopt = { 'menu', 'menuone', 'noselect' }
vim.opt.splitright = true
vim.opt.splitbelow = true
vim.opt.backspace = { 'indent', 'eol', 'start' }
vim.opt.ignorecase = true
vim.opt.wildignorecase = true
vim.opt.wildmode = { 'longest:full', 'full' }
vim.opt.smartcase = true
vim.opt.clipboard:append('unnamedplus')
vim.opt.mouse = 'a'
vim.opt.list = true
vim.opt.listchars = { tab = '»-', trail = '-', eol = '↲', nbsp = '%' }

vim.g.mapleader = ' '
vim.g.maplocalleader = '\\'

local set_noremap = function(mode, lhs, rhs)
  vim.keymap.set(mode, lhs, rhs, { noremap = true, silent = mode ~= 'c' })
end

set_noremap('n', '<C-t>', '<Nop>')
set_noremap('n', '<C-t>n', ':tabnew<CR>')
set_noremap('n', '<C-t>[', ':tabprevious<CR>')
set_noremap('n', '<C-t>]', ':tabnext<CR>')
set_noremap('n', '<ESC><ESC>', ':nohlsearch<CR>')

set_noremap('c', '<C-k>', '<Up>')
set_noremap('c', '<C-j>', '<Down>')

vim.diagnostic.config({
  virtual_text = false,
  signs = true,
  underline = true,
  update_in_insert = true,
  severity_sort = true,
  float = {
    autofocus = false,
    focusable = true,
    focus = false,
    source = true,
    prefix = function(_, i, total)
      return ' [' .. i .. '/' .. total .. '] '
    end,
    scope = 'cursor',
  },
})

set_noremap('n', '<Leader>d', vim.diagnostic.open_float)

require('configure-lazy').configure()
