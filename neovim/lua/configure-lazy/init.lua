local get_lazy_nvim = function()
  local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
  if not (vim.uv or vim.loop).fs_stat(lazypath) then
    vim.fn.system({
      'git',
      'clone',
      '--filter=blob:none',
      'https://github.com/folke/lazy.nvim.git',
      '--branch=stable', -- latest stable release
      lazypath,
    })
  end

  vim.opt.rtp:prepend(lazypath)

  return require('lazy')
end

local configure = function()
  get_lazy_nvim().setup(
    {
      {
        'catppuccin/nvim',
        lazy = false,
        priority = 1000,
        config = function()
          require('catppuccin').setup({
            background = {
              light = 'latte',
              dark = 'macchiato',
            },
            no_italic = true,
          })
        end
      },
      {
        'cormacrelf/dark-notify',
        lazy = false,
        config = function()
          local dn = require('dark_notify')
          dn.run()
          dn.update()
          vim.cmd [[colorscheme catppuccin]]
        end
      },
      { 'justinmk/vim-dirvish',            lazy = false },
      { 'junegunn/fzf',                    build = './install --all', lazy = false },
      { 'junegunn/fzf.vim',                lazy = false },
      { 'nvim-treesitter/nvim-treesitter', build = ':TSUpdate' },
      { 'kevinhwang91/nvim-bqf',           ft = 'qf' },
      {
        'kana/vim-submode',
        keys = { '<C-w>' },
        config = function()
          vim.fn['submode#enter_with']('bufmove', 'n', '', '<C-w>>', '<C-w>>')
          vim.fn['submode#enter_with']('bufmove', 'n', '', '<C-w><', '<C-w><')
          vim.fn['submode#enter_with']('bufmove', 'n', '', '<C-w>+', '<C-w>+')
          vim.fn['submode#enter_with']('bufmove', 'n', '', '<C-w>-', '<C-w>-')
          vim.fn['submode#map']('bufmove', 'n', '', '>', '<C-w>>')
          vim.fn['submode#map']('bufmove', 'n', '', '<', '<C-w><')
          vim.fn['submode#map']('bufmove', 'n', '', '+', '<C-w>+')
          vim.fn['submode#map']('bufmove', 'n', '', '-', '<C-w>-')
        end
      },
      {
        'lewis6991/gitsigns.nvim',
        lazy = false,
        config = function()
          require('gitsigns').setup()
        end
      },
      {
        'neovim/nvim-lspconfig',
        lazy = false,
        config = function()
          vim.api.nvim_create_autocmd('LspAttach', {
            callback = function(args)
              local client = vim.lsp.get_client_by_id(args.data.client_id)

              local opts = { noremap = true, silent = true, buffer = args.buf }

              if client.supports_method('textDocument/implementation') then
                vim.keymap.set('n', '<Leader>gi', vim.lsp.buf.implementation, opts) -- *g*o to *i*mplementation
              end

              --if client.supports_method('textDocument/completion') then
              --  vim.lsp.completion.enable(true, client.id, args.buf, { autotrigger = true })
              --end

              if client.supports_method('textDocument/rename') then
                vim.keymap.set(
                  'n',
                  '<Leader>rn', -- *r*e-*n*ame
                  vim.lsp.buf.rename,
                  opts
                )
              end

              if client.supports_method('textDocument/references') then
                vim.keymap.set('n', '<Leader>gr', vim.lsp.buf.references, opts) -- *g*o to *r*eference
              end

              if client.supports_method('textDocument/formatting') then
                vim.keymap.set(
                  'n',
                  '<Leader>fb', -- *f*ormat *b*uffer
                  function()
                    vim.lsp.buf.format({ bufnr = args.buf, id = client.id })
                  end,
                  opts
                )
              end

              if client.supports_method('textDocument/codeAction') then
                vim.keymap.set('n', '<Leader>ca', vim.lsp.buf.code_action, opts) -- *c*ode *a*ction
              end

              if client.supports_method('textDocument/signatureHelp') then
                vim.keymap.set('i', '<C-s>', vim.lsp.buf.signature_help, opts) -- *s*ignature help
              end
            end,
          })

          -- Reference: https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
          local lspconfig = require('lspconfig')

          lspconfig.lua_ls.setup {}
          lspconfig.pyright.setup {}
          lspconfig.ts_ls.setup {}
          lspconfig.eslint.setup {}
          lspconfig.sourcekit.setup{}
          lspconfig.kotlin_language_server.setup{}
        end
      },
    },
    {
      defaults = {
        lazy = true,
      },
    }
  )
end

return {
  configure = configure,
}
