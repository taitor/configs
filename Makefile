DIR = $(shell pwd)
CONFIG_HOME ?= $(or $(XDG_CONFIG_HOME),$(HOME)/.config)

.PHONY: install
install: zsh neovim tmux alacritty

.PHONY: zsh
zsh:
	test -f $(HOME)/.zshrc || ln -s $(DIR)/.zshrc $(HOME)/.zshrc

.PHONY: neovim
neovim: xdg_config_home
	test -d $(CONFIG_HOME)/nvim || ln -s `pwd`/neovim $(CONFIG_HOME)/nvim

.PHONY: tmux
tmux:
	test -f $(HOME)/.tmux.conf || ln -s $(DIR)/.tmux.conf $(HOME)/.tmux.conf

.PHONY: alacritty
alacritty: xdg_config_home
	test -d $(CONFIG_HOME)/alacritty || ln -s $(DIR)/alacritty $(CONFIG_HOME)/alacritty
	cp $(DIR)/alacritty/alacritty-light.toml $(DIR)/alacritty/alacritty.toml

.PHONY: xdg_config_home
xdg_config_home:
	mkdir -p $(CONFIG_HOME)
